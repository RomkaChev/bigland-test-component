<?php
namespace romkachev\bigland\test\component;

use yii\base\Component;
use yii\caching\Cache;
use yii\di\Instance;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * Class RussianRegistryAttributesParser
 *
 * @package romkachev\bigland\test\component
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class RussianRegistryAttributesParser extends Component
{
    /** @var Cache */
    public $cache = 'cache';

    /** @var int */
    public $cacheDuration = 3600;

    /** @var bool */
    public $cacheEnabled = true;

    /** @var Client */
    public $httpClient = [
        'class'     => '\yii\httpclient\Client',
        'transport' => '\yii\httpclient\CurlTransport',
    ];

    /**
     * {@inheritdoc}
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        $this->cache = Instance::ensure($this->cache, Cache::className());

        if (!$this->httpClient instanceof Client) {
            $this->httpClient = \Yii::createObject($this->httpClient);
        }

        parent::init();
    }

    /**
     * @param string|array $cns
     *
     * @return CadastralPlot[]
     */
    public function parse($cns)
    {
        $plots = [];
        $cns   = $this->prepareInput($cns);

        foreach ($cns as $i => $cn) {
            if ($this->cacheEnabled && ($cadastralPlot = $this->cache->get($this->getCacheKey($cn))) !== false) {
                $plots[$cn] = $cadastralPlot;
                ArrayHelper::remove($cns, $i);
            }
        }

        $requests = [];
        foreach ($cns as $cn) {
            $requests[$cn] = $this->httpClient->get("http://pkk5.rosreestr.ru/api/features/1/{$cn}");
        }

        $responses = $this->httpClient->batchSend($requests);
        foreach ($responses as $cn => $response) {
            if ($response->isOk) {
                $cadastralPlot = (new CadastralPlot())->loadViaRussianRegistryData($response->getData());
                $plots[$cn]    = $cadastralPlot;

                if ($this->cacheEnabled) {
                    $this->cache->set($this->getCacheKey($cn), $cadastralPlot, $this->cacheDuration);
                }
            }
        }

        return $plots;
    }

    /**
     * @param string|array $cns
     *
     * @return array
     */
    private function prepareInput($cns)
    {
        if (is_string($cns)) {
            $cns = explode(",", $cns);
        }

        $cns = array_map('trim', $cns);
        $cns = array_filter($cns);
        $cns = array_unique($cns);
        $cns = array_map(function ($cn) {
            $pieces = array_map(function ($piece) {
                return ltrim($piece, '0');
            }, explode(':', $cn));

            return implode(':', $pieces);
        }, $cns);
        return $cns;
    }

    /**
     * @param string $cn
     *
     * @return array
     */
    public function getCacheKey($cn)
    {
        return ['cadastral-plot-attributes', 'cn' => $cn];
    }
}