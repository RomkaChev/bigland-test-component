<?php
namespace romkachev\bigland\test\component\tests\unit\tests;

use bigland\geo\russianRegistry\tests\unit\BaseTestCase;
use romkachev\bigland\test\component\RussianRegistryAttributesParser;

/**
 * Class Test
 *
 * @package romkachev\bigland\test\component\tests\unit
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class RussianRegistryAttributesParserTest extends BaseTestCase
{
    public function testParse()
    {
        $parser         = new RussianRegistryAttributesParser();
        $cadastralPlots = $parser->parse('69:27:0000022:1306');
        $cadastralPlot  = $cadastralPlots['69:27:22:1306'];

        $this->assertEquals('69:27:0000022:1306', $cadastralPlot->cadastralNumber);
        $this->assertEquals(37578.06, $cadastralPlot->price);
        $this->assertEquals(10035.00, $cadastralPlot->area);
        $this->assertEquals('Тверская область, р-н Ржевский, с/пос "Успенское", северо-западнее д. Горшково из земель СПКколхоз "Мирный"', $cadastralPlot->address);
    }
}
