<?php
namespace bigland\geo\russianRegistry\tests\unit;

use yii\console\Application;

/**
 * Class BaseTestCase
 *
 * @package bigland\geo\russianRegistry\tests\unit
 *
 * @author  Roman Kulikov <r.v.kulikov@yandex.ru>
 */
class BaseTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @inheritdoc
     */
    protected function setUp()
    {
        new Application(require(__DIR__ . '/config.php'));
    }
}