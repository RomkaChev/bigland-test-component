<?php
namespace romkachev\bigland\test\component;

use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class CadastralPlot
 *
 * @package romkachev\bigland\test\component
 *
 * @author  Kulikov Roman <r.v.kulikov@yandex.ru>
 */
class CadastralPlot extends Model
{
    /** @var string */
    public $cadastralNumber;

    /** @var string */
    public $address;

    /** @var float */
    public $price;

    /** @var float */
    public $area;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['cadastralNumber', 'string'],
            ['address', 'string'],
            ['price', 'number'],
            ['area', 'number'],
        ];
    }

    /**
     * @param array $data
     *
     * @return CadastralPlot
     */
    public function loadViaRussianRegistryData($data)
    {
        $this->cadastralNumber = ArrayHelper::getValue($data, 'feature.attrs.cn');
        $this->address         = ArrayHelper::getValue($data, 'feature.attrs.address');
        $this->price           = ArrayHelper::getValue($data, 'feature.attrs.cad_cost');
        $this->area            = ArrayHelper::getValue($data, 'feature.attrs.area_value');

        return $this;
    }
}